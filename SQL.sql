/*
Navicat MySQL Data Transfer

Source Server         : phpmyadmin
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : azure

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2018-06-07 08:36:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for xdr_users
-- ----------------------------
DROP TABLE IF EXISTS `xdr_users`;
CREATE TABLE `xdr_users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `birth` varchar(10) NOT NULL DEFAULT '01-01-2013',
  `rpx_id` varchar(200) NOT NULL DEFAULT '',
  `rpx_type` enum('0','1','2') NOT NULL DEFAULT '0',
  `homeBg` varchar(120) NOT NULL DEFAULT 'b_bg_pattern_abstract2',
  `web_online` varchar(100) NOT NULL DEFAULT '',
  `visibility` enum('NOBODY','EVERYONE') NOT NULL DEFAULT 'EVERYONE',
  `predermited` enum('0','1') NOT NULL DEFAULT '1',
  `client_token` varchar(51) NOT NULL DEFAULT '',
  `token` varchar(21) NOT NULL DEFAULT '',
  `AddonData` text NOT NULL,
  `AccountID` bigint(255) NOT NULL DEFAULT '0',
  `AccountName` varchar(255) NOT NULL DEFAULT '',
  `receptionPased` enum('0','1') NOT NULL DEFAULT '0',
  `RememberMeToken` text NOT NULL,
  `vinculId` int(255) NOT NULL DEFAULT '0',
  `browser` varchar(255) NOT NULL DEFAULT '',
  `securityTokens` text NOT NULL,
  `country_cf` varchar(100) NOT NULL,
  `ip_current` varchar(45) NOT NULL,
  `task` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `mail` (`mail`) USING HASH,
  KEY `password` (`password`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdr_users
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_acp_comments
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_acp_comments`;
CREATE TABLE `xdrcms_acp_comments` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `userID` int(255) NOT NULL,
  `Created` int(255) NOT NULL,
  `Title` varchar(100) NOT NULL,
  `Body` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_acp_comments
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_addons
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_addons`;
CREATE TABLE `xdrcms_addons` (
  `id` int(220) NOT NULL AUTO_INCREMENT,
  `title` varchar(61) NOT NULL,
  `category` int(5) NOT NULL DEFAULT '1',
  `color` int(1) NOT NULL DEFAULT '0',
  `rank` int(1) NOT NULL DEFAULT '0',
  `internal` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `template` enum('0','1') NOT NULL DEFAULT '1',
  `creatorId` int(255) NOT NULL DEFAULT '0',
  `created` int(255) NOT NULL DEFAULT '0',
  `canDisable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_addons
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_aio_config
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_aio_config`;
CREATE TABLE `xdrcms_aio_config` (
  `Name` varchar(255) NOT NULL,
  `Data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_aio_config
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_categories
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_categories`;
CREATE TABLE `xdrcms_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_categories
-- ----------------------------
INSERT INTO `xdrcms_categories` VALUES ('1', 'Ocultos');
INSERT INTO `xdrcms_categories` VALUES ('2', 'Staffs');
INSERT INTO `xdrcms_categories` VALUES ('3', 'Alfas');

-- ----------------------------
-- Table structure for xdrcms_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_guestbook`;
CREATE TABLE `xdrcms_guestbook` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `message` varchar(500) CHARACTER SET latin1 DEFAULT NULL,
  `time` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `widget_id` int(10) DEFAULT NULL,
  `userid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_looks
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_looks`;
CREATE TABLE `xdrcms_looks` (
  `Look` text CHARACTER SET latin1 NOT NULL,
  `Gender` enum('M','F') CHARACTER SET latin1 NOT NULL,
  `Code` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_looks
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_minimail
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_minimail`;
CREATE TABLE `xdrcms_minimail` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `OwnerId` int(255) NOT NULL DEFAULT '0',
  `SenderId` int(255) NOT NULL DEFAULT '0',
  `ToIds` varchar(255) NOT NULL DEFAULT '0',
  `IsReaded` tinyint(1) NOT NULL DEFAULT '0',
  `InBin` tinyint(1) NOT NULL DEFAULT '0',
  `RelatedId` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL DEFAULT 'Test Message',
  `Message` text,
  `Created` int(14) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_minimail
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_minimail_campaigns
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_minimail_campaigns`;
CREATE TABLE `xdrcms_minimail_campaigns` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `Creator` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL DEFAULT 'Hello',
  `Message` text,
  `Image` varchar(255) NOT NULL DEFAULT 'http://images.habbo.com/c_images/album3236/em_wbk_2.gif',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_minimail_campaigns
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_news
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_news`;
CREATE TABLE `xdrcms_news` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(255) NOT NULL,
  `Category` int(2) NOT NULL DEFAULT '1',
  `Summary` text NOT NULL,
  `Body` text NOT NULL,
  `BackGroundImage` varchar(255) NOT NULL DEFAULT '',
  `Images` text NOT NULL,
  `TimeCreated` int(255) NOT NULL DEFAULT '0',
  `Visible` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_news
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_permissions
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_permissions`;
CREATE TABLE `xdrcms_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_permissions
-- ----------------------------
INSERT INTO `xdrcms_permissions` VALUES ('1', 'ase.access');
INSERT INTO `xdrcms_permissions` VALUES ('2', 'ase.site');
INSERT INTO `xdrcms_permissions` VALUES ('3', 'ase.server');
INSERT INTO `xdrcms_permissions` VALUES ('4', 'ase.swfs');
INSERT INTO `xdrcms_permissions` VALUES ('5', 'ase.users_page');
INSERT INTO `xdrcms_permissions` VALUES ('6', 'ase.catalog');
INSERT INTO `xdrcms_permissions` VALUES ('7', 'ase.shop_page');
INSERT INTO `xdrcms_permissions` VALUES ('8', 'ase.logs');
INSERT INTO `xdrcms_permissions` VALUES ('9', 'ase.edit_delete');
INSERT INTO `xdrcms_permissions` VALUES ('10', 'ase.delete_logs');
INSERT INTO `xdrcms_permissions` VALUES ('11', 'ase.edit_users');
INSERT INTO `xdrcms_permissions` VALUES ('12', 'ase.give_rank');
INSERT INTO `xdrcms_permissions` VALUES ('13', 'ase.ban_unban');
INSERT INTO `xdrcms_permissions` VALUES ('14', 'ase.alerts');
INSERT INTO `xdrcms_permissions` VALUES ('15', 'ase.uploads');
INSERT INTO `xdrcms_permissions` VALUES ('16', 'ase.articles');

-- ----------------------------
-- Table structure for xdrcms_plugins
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_plugins`;
CREATE TABLE `xdrcms_plugins` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `Position` int(5) DEFAULT NULL,
  `minRank` int(5) DEFAULT NULL,
  `Template` enum('0','1') DEFAULT '1',
  `ownerID` int(5) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `canDisable` enum('0','1') DEFAULT '1',
  `canDelete` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_plugins
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_promos
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_promos`;
CREATE TABLE `xdrcms_promos` (
  `Id` int(255) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(255) NOT NULL DEFAULT '0',
  `Title` varchar(80) NOT NULL DEFAULT '',
  `Content` text NOT NULL,
  `BackGroundImage` text NOT NULL,
  `GoToHTML` text NOT NULL,
  `TimeCreated` varchar(255) NOT NULL DEFAULT '0-0-0',
  `Visible` enum('0','1') NOT NULL DEFAULT '1',
  `Button` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_promos
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_ranks
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_ranks`;
CREATE TABLE `xdrcms_ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT '1',
  `ase.1` enum('0','1') DEFAULT '0',
  `ase.2` enum('0','1') DEFAULT '0',
  `ase.3` enum('0','1') DEFAULT '0',
  `ase.4` enum('0','1') DEFAULT '0',
  `ase.5` enum('0','1') DEFAULT '0',
  `ase.6` enum('0','1') DEFAULT '0',
  `ase.7` enum('0','1') DEFAULT '0',
  `ase.8` enum('0','1') DEFAULT '0',
  `ase.9` enum('0','1') DEFAULT '0',
  `ase.10` enum('0','1') DEFAULT '0',
  `ase.11` enum('0','1') DEFAULT '0',
  `ase.12` enum('0','1') DEFAULT '0',
  `ase.13` enum('0','1') DEFAULT '0',
  `ase.14` enum('0','1') DEFAULT '0',
  `ase.15` enum('0','1') DEFAULT '0',
  `ase.16` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=ascii;

-- ----------------------------
-- Records of xdrcms_ranks
-- ----------------------------
INSERT INTO `xdrcms_ranks` VALUES ('1', 'Tecnico', '8', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `xdrcms_ranks` VALUES ('8', 'Encargado de Publicidad', '7', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `xdrcms_ranks` VALUES ('9', 'Publicista', '6', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('10', 'Lince', '5', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `xdrcms_ranks` VALUES ('11', 'Gu?a', '4', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for xdrcms_shop
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_shop`;
CREATE TABLE `xdrcms_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('item','badge','voucher') NOT NULL DEFAULT 'item',
  `code` varchar(200) DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `limit` int(11) NOT NULL DEFAULT '1',
  `current` int(11) NOT NULL DEFAULT '0',
  `enabled` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of xdrcms_shop
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_staff_log
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_staff_log`;
CREATE TABLE `xdrcms_staff_log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `action` varchar(12) NOT NULL,
  `message` text,
  `note` text,
  `userid` int(23) NOT NULL DEFAULT '0',
  `targetid` int(11) DEFAULT '0',
  `timestamp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xdrcms_staff_log
-- ----------------------------

-- ----------------------------
-- Table structure for xdrcms_updates
-- ----------------------------
DROP TABLE IF EXISTS `xdrcms_updates`;
CREATE TABLE `xdrcms_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('0',' 1',' 2','3') DEFAULT '3',
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of xdrcms_updates
-- ----------------------------
INSERT INTO `xdrcms_updates` VALUES ('1', '0', 'Nuevo Diseño \'iJollyness\'', 'iJollyness la sensación de aXDR.', '2018-05-18');
INSERT INTO `xdrcms_updates` VALUES ('2', '0', 'Nuevo Sistema de Rango', 'Se empieza un nuevo sistema de Rangos para la CMS.', '2018-05-18');
INSERT INTO `xdrcms_updates` VALUES ('3', ' 2', '&iexcl;ASE Sensacional!', 'Ahora la All Seeing Eye 0.5 Beta te permitirá realizar movimientos rapidos.', '2018-05-18');
INSERT INTO `xdrcms_updates` VALUES ('4', '3', 'Probando Sistema de Updates', 'Este sistema sera muy util', '2018-05-07');
SET FOREIGN_KEY_CHECKS=1;

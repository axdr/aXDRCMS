<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright © 2015 Xdr.
|+=========================================================+
|| # Xdr 2015. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

const STYLE_OTHER = 'Jollyness';
//header('Location: me'); exit;

const Maintenance = true;
require '../KERNEL-XDRCMS/Init.php';

Site::Redirect(Redirect::BLOCKED);
Site::$PageName = Title::Shop[0];
Site::$PageColor = (Title::FixedColor) ? Title::Color : Title::Shop[1];
Site::$PageId = 'shop';

$tabId = (int) isset($_GET['tab']) && is_numeric($_GET['tab']) ? $_GET['tab'] : 1;

require HEADER . 'Community.php';

switch ($tabId)
{	
	case 1:
	case 2:
	case 3:
		require HTML . 'Shop_page_' . $tabId . '.html';
		break;
	default:
		require HTML . 'Shop_page_1.html';
		break;
};


require FOOTER . 'Login.php';

?>
aXDR CMS: TODO List
=============                    

### BUG FIXING:

- No se pueden editar Plugins -> Si se puede, pero necesitas dar click en los check box
- No puedes guardar ajustes
- Las "últimas" noticias de estilo Habbo no se muestran en pagina NOTICIAS
- No te puedes registrar en el estilo Habbo -> Listo

### ALTA PRIORIDAD:
- [X] [iJollyness] Terminar de programar las noticias
- [X] [iJollyness] Terminar la pagina de Ajustes
- [X] Nuevo sistema de Rangos
- [X] Banear/Desbanear Usuario

- [ASE] Agregar 'Definir Home Room' al registrar
- [ASE] Agregar 'Actualizar Home Room' de los usuarios en grupo e individualmente.

- [Todos] Crear pagina de Reglamento
- [ASE] Programar el Contador de Usuarios por Dia
- [ASE] Programar Acciones Rapidas
- [ASE] Subir Placas, MPUs, Ropa
- [ASE] Registrar Updates
- [ASE] Programar Ver mis logs

### MEDIA PRIORIDAD:
- [iJollyness] Rediseñar Pagina Staff
- [iJollyness] Terminar la comunidad en diseño
- [iJollyness] Terminar los TOPs
- [Jollyness] Rediseñar Registro
- [ASE] Crear Pagina de Registro de Updates (Lo que muestra en el login)

### BAJA PRIORIDAD:
- [iJollyness] Mejorar diseño de las forms de Ajustes
- [Todos] Rediseñar página de Articulos Añadir ver todas las noticias y navegar por todas.
- [X] Agregar Verificacion de Tablas en ASE > Usuarios > Monedas - Hecho
- [X] Agregar Verificacion de Tablas en ASE > Usuarios > Bans - Hecho
- [Jollyness] Rehacer CSS y HTML

aXDR CMS Professional Version
---------------------

### Actualizaciones MENORES:
- [X] Crear Accordions ASE 
- [X] Rehacer Sistema de Articulos
- [X] Actualizado ReCaptcha v2
- Asegurar el funcionamiento de Secret Key
- Mejorar Search aXDR 0.4 Beta, script KeyUp
- Mejorar pagina de SWF Overrides
- Crear pagina de DEBUG
- [Todos] Buscador de Usuarios en la WEB (keyUp)
- [ASE] Buscador por País
- [Todos] Agregar en Tops: Top Países
- [ASE] Agregar/Borrar/Editar Rangos & Permisos de WEB
- [ASE] Configurar Looks de Registro (Hombre / Mujer)
- Mover los updates a Cache
- [X] [ASE] Crear menu despegable

### Actualizaciones MAYORES:
- Sistema de Perfiles
- [X] Sistema de Plugins - 90% hecho
- Sistema de Tienda
- Sistema de Recuperacion de Contraseñas
- Sistema de Client Paralelo
- Sistema Multi-Emulador - Ya empezado 65% hecho
- Rehacer Sistema de Mantenimiento
- Sistema #Tags
- Sistema de limite de IPs
- Sistema de Eventos:
    "Titulo de Evento:"
    "Categoria:" => (Juego, Concurso, Oleada)
    "Empieza:" 
    "Termina:"
    "Id de Sala" => (Con el ID de la sala obtienes el nombre de la sala y su dueño)
    "Dificultad"
    "Participantes/Ganadores" => (Buscador de Usuarios [keyUp])
- Sistema de 'Proximamente'
- Sistema de Diseños (Plantillas)
- Sistema de "Historias" (Config::$Options['camera']['enabled'])
- Sistema Avanzado de Controlador de Paginas